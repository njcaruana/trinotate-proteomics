#!/usr/bin/env ruby

require 'sqlite3'
require 'bio'
require 'optparse'

# User friendly command-line options
#
options = {}
optparser=OptionParser.new do |opts|
	opts.banner = "Usage: orf_to_fasta.rb trinotatedb.sqlite [options]"

	opts.on("-o filename", "--output filename", "Write output to file") do |f|
		options[:output_file] = f
	end

	options[:blast_compatible] = false
	opts.on("-b","--blast-compatible","Produce BLAST compatible ids by replacing cds. with lcl|") do
		options[:blast_compatible] = true
	end

	options[:go_terms] = false
	opts.on("-g","--go-terms","Include GO Terms in the protein descriptions") do
		options[:go_terms] = true
	end

	options[:eggnog] = false
	opts.on("-e","--eggnog","Include EggNOG Terms in the protein descriptions") do
		options[:eggnog] = true
	end

	options[:hit_info] = false
	opts.on("-h","--hit-info","Include Hit Info in the protein descriptions") do
		options[:hit_info] = true
	end

	opts.on_tail("-h", "--help", "Show this message") do
		puts opts
		exit
	end

end

optparser.parse!

# Checking that the user at least supplied the name of the sqlite file
#
if ARGV.length==0
	puts optparser 
	exit
end

# Input and output files
#
if options[:output_file]
	output_file=File.new(options[:output_file],'w') 	
else
	output_file=$stdout
end
input_file_name=ARGV.shift 


def swissprot_entry_from_accession(db,accession)
	results=db.execute("select LinkID from UniprotIndex where Accession=\"#{accession}\"")
	results.join(" ")
end

orf_dictionary = {}

db=SQLite3::Database.open (input_file_name)

	db.execute("select orf_id,FullAccession,peptide,PercentIdentity,min(Evalue) from ORF LEFT JOIN BlastDbase ON ORF.orf_id=BlastDbase.TrinityID GROUP BY orf_id") do |row|

	orfid = row[0] 
	full_accession = row[1]
	peptide = row[2]

	if full_accession!=nil
		accession_number=full_accession.split("|")[-2]
		accession_number=accession_number.split(".")[0]
		swissprot_entry=swissprot_entry_from_accession(db,accession_number)
		swissprot_entry.gsub!(/GO:\d+/,"") unless options[:go_terms]
		swissprot_entry.gsub!(/NOG\d+/,"") unless options[:eggnog]
		blast_info = "Identity: #{row[3]} EValue:#{row[4]}"
	else
		swissprot_entry=""
		blast_info=""
	end

	orfid.sub!("\|","_").sub!("cds.","lcl\|") if options[:blast_compatible]
	blast_info="" unless options[:hit_info]

	fastaentry = Bio::FastaFormat.new(">#{orfid} #{swissprot_entry.sub("RecName:","").sub("Full=","")} #{blast_info}\n#{peptide.gsub("*","")}")

	output_file.write fastaentry

end
