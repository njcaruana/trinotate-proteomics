#!/usr/bin/env ruby

require 'sqlite3'
require 'bio'
require 'optparse'

# User friendly command-line options
#
options = {}
optparser=OptionParser.new do |opts|
	opts.banner = "Usage: orf_to_go.rb trinotatedb.sqlite [options]"

	opts.on("-o filename", "--output filename", "Write output to file") do |f|
		options[:output_file] = f
	end

	opts.on_tail("-h", "--help", "Show this message") do
		puts opts
		exit
	end

end

optparser.parse!

# Checking that the user at least supplied the name of the sqlite file
#
if ARGV.length==0
	puts optparser 
	exit
end

# Input and output files
#
if options[:output_file]
	output_file=File.new(options[:output_file],'w') 	
else
	output_file=$stdout
end
input_file_name=ARGV.shift 


def go_terms_from_accession(db,accession)
	results=db.execute("select LinkID from UniprotIndex where Accession=\"#{accession}\"")
	results=results.flatten.keep_if {|item| item =~ /^GO:/}
	results.join(", ")
end

orf_dictionary = {}

db=SQLite3::Database.open (input_file_name)

	db.execute("select orf_id,FullAccession from ORF LEFT JOIN BlastDbase ON ORF.orf_id=BlastDbase.TrinityID GROUP BY orf_id") do |row|

	orfid = row[0] 
	full_accession = row[1]

	if full_accession!=nil
		accession_number=full_accession.split("|")[-2]
		accession_number=accession_number.split(".")[0]
		# require 'debugger';debugger
		go_terms=go_terms_from_accession(db,accession_number)
	else
		go_terms=""
	end

	output_file.write "#{orfid}\t#{go_terms}\n"

end
