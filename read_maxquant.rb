#!/usr/bin/env ruby

#reads protein file and converts needed columns into csv file. 


file = File.open( 'mb259_2_proteinGroups.txt')

groupid = 0

#puts "GroupID,ProteinID,PeptideCount,iBAQAcetone,RawIntensity,lfqIntensity,SpectralCount"

file.each_line do |row|

	columns = row.split("\t")
	protein_ids=columns[0].split(";")
	peptide_counts=columns[2].split(";")
	sequence_lengths=columns[39].split(";")
	protein_index=0
	#sequence_index=0
	protein_ids.each do |protein_id|

		if protein_id=~ /^cds\./ 
			puts "#{groupid},#{protein_id},#{"PROTEIN"},#{"1"},#{columns[37]},#{sequence_lengths[protein_index]},#{peptide_counts[protein_index]},#{columns[67]},#{columns[58]}, #{columns[75]}, #{columns[83]}"
		end
		protein_index += 1
		#sequence_index += 1
	end
	
	groupid += 1

end 


