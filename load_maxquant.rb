#!/usr/bin/env ruby

require 'sqlite3'
require 'bio'
require 'optparse'
require_relative 'sql_util'

# User friendly command-line options
#
options = {}
optparser=OptionParser.new do |opts|
	opts.banner = "Usage: load_maxquant.rb trinotatedb.sqlite proteinGroups.txt [options]"

	opts.on("-s name,tissue", "--sample name,tissue", "Sample information for the data. If sample exists then data will be appended.") do |s|
		options[:sample] = s
	end

	opts.on("-r sep,enz","--run name,sep,enz,instrument","Run information for the data.") do |r|
		options[:run] = r
	end

	opts.on("-q col1,col2","--quant-columns col1,col2","Columns to use for quant value. Average of all columns provided will be used") do |r|
		options[:quant_columns] = r
	end

	opts.on_tail("-h", "--help", "Show this message") do
		puts opts
		exit
	end
end

optparser.parse!

# Checking that the user at least supplied the name of the sqlite file and a tabular file
#
if ARGV.length<2
	puts optparser 
	exit
end

throw "You must supply a sample" unless options[:sample]
throw "You must supply run info" unless options[:run]

db_file_name=ARGV.shift 
input_file_name=ARGV.shift

db=SQLite3::Database.open (db_file_name)

create_mq_table(db)
create_msrun_table(db)
create_mssample_table(db)

sample_name = create_sample_if_needed(db,options)

run_name = create_run_if_needed(db,sample_name,options)

# TODO: Create a temp file and use an import statement to make this faster. 
# Running a query for every line is slow but not unacceptable for most maxquant files.
#

quant_column_names = options[:quant_columns].split(",")
abundance_columns = []
coverage_column = nil
num_peptides_column = nil

File.foreach(input_file_name) do |line|

	line_parts = line.chomp.split("\t")

	if line_parts[0]=='Protein IDs' #Header line. Use this to figure out columns for quantitation
		line_parts.each_with_index { |e, i| 
			abundance_columns<<i if quant_column_names.include? e 
			coverage_column=i if e=="Sequence coverage [%]"
			num_peptides_column=i if e=="Peptide counts (all)"
		}
	else

		row_values = [run_name]

		protein_ids = line_parts[0].split(";")
		row_values << protein_ids[0]

		indistinguishable_proteins = protein_ids.join(";")
		row_values << indistinguishable_proteins

		abundance = 0
		abundance_columns.each { |e| abundance+=line_parts[e.to_i].to_f }	
		row_values << abundance

		row_values << quant_column_names.join(";")

		row_values << line_parts[coverage_column].to_f
		row_values << line_parts[num_peptides_column].to_i

		db.execute("insert into MaxQuant values (?,?,?,?,?,?,?)",row_values)		
	end
end


