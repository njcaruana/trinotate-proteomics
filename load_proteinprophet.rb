#!/usr/bin/env ruby

require 'sqlite3'
require 'bio'
require 'optparse'
require_relative 'sql_util'

# User friendly command-line options
#
options = {}
optparser=OptionParser.new do |opts|
	opts.banner = "Usage: load_proteinprophet.rb trinotatedb.sqlite protxml.tsv [options]"

	opts.on("-s name,tissue", "--sample name,tissue", "Sample information for the data. If sample exists then data will be appended.") do |s|
		options[:sample] = s
	end

	opts.on("-r sep,enz","--run name,sep,enz,instrument","Run information for the data.") do |r|
		options[:run] = r
	end

	opts.on("--id-regex re","Regex for parsing orf or transcript IDs from protein IDs") do |r|
		options[:id_regex] = r
	end	

	opts.on_tail("-h", "--help", "Show this message") do
		puts opts
		exit
	end

end

optparser.parse!

# Checking that the user at least supplied the name of the sqlite file and a tabular file
#
if ARGV.length<2
	puts optparser 
	exit
end

throw "You must supply a sample" unless options[:sample]
throw "You must supply run info" unless options[:run]


db_file_name=ARGV.shift 
input_file_name=ARGV.shift

db=SQLite3::Database.open (db_file_name)

create_pp_table(db)
create_msrun_table(db)
create_mssample_table(db)

sample_name = create_sample_if_needed(db,options)

run_name = create_run_if_needed(db,sample_name,options)

# TODO: Create a temp file and use an import statement to make this faster. 
# Running a query for every line is slow but not unacceptable for most proteinprophet files.
#
File.foreach(input_file_name) do |line|

	line_parts = line.chomp.split("\t")
	if line_parts[0]!="group_number"
		if options[:id_regex]
			# require 'debugger';debugger
			line_parts[2]= /#{options[:id_regex]}/.match(line_parts[2])[1]
		end
		line_parts.delete_at(9)
		line_parts.delete_at(3)

		db.execute("insert into ProteinProphet values (?,?,?,?,?,?,?,?,?)",run_name,line_parts)		
	end

end


