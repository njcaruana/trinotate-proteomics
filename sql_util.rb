def create_mssample_table(db)
	result = db.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='MSSample'")
	if result.length==0
		db.execute("CREATE TABLE MSSample(sample_name PRIMARY KEY,tissue)")
	end
end

def create_msrun_table(db)
	result = db.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='MSRun'")
	if result.length==0
		db.execute("CREATE TABLE MSRun(run_id PRIMARY KEY,separation,enzyme,instrument,sample_name)")
	end
end

def create_pp_table(db)
	result = db.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='ProteinProphet'")
	if result.length==0
		db.execute("CREATE TABLE ProteinProphet (run_id,group_id INTEGER,group_prob REAL,\
			protein_id,indistinguishable_proteins,protein_probability REAL,\
			coverage REAL,peptides,num_peptides INTEGER)")
	end
end

def create_mq_table(db)
	result = db.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='MaxQuant'")
	if result.length==0
		db.execute("CREATE TABLE MaxQuant (run_id,\
			protein_id,indistinguishable_proteins,\
			abundance REAL,abundance_measure,
			coverage REAL,num_peptides INTEGER)")
	end
end


def create_sample_if_needed(db,options)
	sample_info = options[:sample].split(",").collect { |e| e.chomp }

	sample_name = sample_info[0]

	sample_id = db.execute("select sample_name from MSSample where sample_name==\"#{sample_name}\"").first

	if sample_id.nil?
		# Create sample id if it doesn't exist already
		throw "Attempt to create new sample but no tissue type provided" if sample_info.length<2
		tissue = sample_info[1]
		db.execute("insert into MSSample (sample_name,tissue) values (?,?)",sample_name,tissue)
	end
	sample_name
end

def create_run_if_needed(db,sample_name,options)
	run_info = options[:run].split(",").collect { |e| e.chomp }

	run_name = run_info[0]

	run_id = db.execute("select run_id from MSRun where run_id==\"#{run_name}\"").first

	if run_id.nil?
		# Create sample id if it doesn't exist already
		throw "You must provide run data as name,separation,enzyme,instrument" unless run_info.length==4
		db.execute("insert into MSRun values (?,?,?,?,?)",run_info,sample_name)
	end
	run_name
end
