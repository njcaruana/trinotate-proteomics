#!/usr/bin/env ruby

require 'sqlite3'
require 'bio'
require 'optparse'

# User friendly command-line options
#
options = {}
optparser=OptionParser.new do |opts|
	opts.banner = "Usage: transcript_to_fasta.rb trinotatedb.sqlite [options]"

	opts.on("-o filename", "--output filename", "Write output to file") do |f|
		options[:output_file] = f
	end

	options[:include_go]=FALSE
	opts.on("-g", "--goterms", "Include GO terms in fasta header") do
		options[:include_go] = TRUE
	end

	opts.on_tail("-h", "--help", "Show this message") do
		puts opts
		exit
	end

end

optparser.parse!

# Checking that the user at least supplied the name of the sqlite file
#
if ARGV.length==0
	puts optparser 
	exit
end

# Input and output files
#
if options[:output_file]
	output_file=File.new(options[:output_file],'w') 	
else
	output_file=$stdout
end
input_file_name=ARGV.shift 


def swissprot_entry_from_accession(db,accession)
	results=db.execute("select LinkID from UniprotIndex where Accession=\"#{accession}\"")
	unless results.length==0
		results[1][0]="Length: #{results[1][0]}"
	end
	results.join(" ")
end

orf_dictionary = {}

db=SQLite3::Database.open (input_file_name)

	db.execute("select transcript_id,FullAccession,sequence,PercentIdentity,min(Evalue) from Transcript LEFT JOIN BlastDbase ON Transcript.transcript_id=BlastDbase.TrinityID GROUP BY transcript_id") do |row|

	tsid = row[0] 
	full_accession = row[1]
	sequence = row[2]
	percent_identity = row[3]
	eval=row[4]

	if full_accession!=nil
		accession_number=full_accession.split("|")[-2]
		accession_number=accession_number.split(".")[0]
#		require 'debugger';debugger
		swissprot_entry=swissprot_entry_from_accession(db,accession_number)
		swissprot_entry.sub!(/^RecName: Full=/,"")
		unless options[:include_go]
			swissprot_entry.gsub!(/GO:\d+/,"")
		end
		blast_info = "Identity: #{percent_identity}% EValue:#{row[4]}"
	else
		swissprot_entry=""
		blast_info=""
	end
	fastaentry = Bio::FastaFormat.new(">#{tsid} #{swissprot_entry} #{blast_info}\n#{sequence}")

	output_file.write fastaentry

end
