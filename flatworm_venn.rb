#!/usr/bin/env ruby

#Script to create venn diagram of Swiss Prot identifiers in flatworm compared with own data.
#Save Trinotate excel file as tab delimited
#Read in array of slime proteins (find file)
#Read the excel file 1 line at a time - at each line split by the tab, get the column.
#If the cds is in the array, then print the line.
#Cat. the swiss prot identifier between the |...|
#Use that list against the identifiers from flat worm and create a venn diagram in R.


# This probably works, but I would recommend against using full paths as your code will break if you ever rearrange files
# I've left your code as-is .. but a better approach would be to do
#f =File.open("mb259_2_allorfs.txt")
# And then run always your script from within the Venn_Diagram directory

f =File.open('/Users/nikeishacaruana/Dropbox/Trinotate_Proteomics/Venn_Diagram/mb259_2_allorfs.txt')

# Yep .. good
protein_array = []

# First ... just create the protein_array.  Don't try to do other steps in this loop
f.each_line do |line|
	protein_array << line
end


# Now that you have the protein array loop over the annotation file to grab swissprot identifiers
#

# Ditto .. comments above
trinotate=File.open("/Users/nikeishacaruana/Dropbox/Trinotate_Proteomics/Venn_Diagram/trinotate_annotation_report.txt")
	
trinotate.each_line do |row|
	row.split("\t")
	protein_id=row[0]
	puts protein_id
end

# This logic needs to be inside the loop above
#
# if protein_id = protein_array
# 	then
# 	puts protein_id
# end
