#!/usr/bin/env ruby

# This script queries the database and dumps out information on all proteins with MS identifications
# Output is tab separated and includes columns for all basic protein info as well as additional 
# information relevant to a toxin analysis

require 'sqlite3'
require 'bio'
require 'optparse'
require 'arrayfields'
require_relative 'sql_util'

options = {}
optparser=OptionParser.new do |opts|
	opts.banner = "Usage: get_toxintable.rb Trinotate.sqlite [options]"

	options[:by_transcript]=false
	opts.on_tail("-t", "--by-transcript", "Protein IDs match assumed to match transcripts not ORFs") do
		options[:by_transcript]=true
	end

	opts.on_tail("-h", "--help", "Show this message") do
		puts opts
		exit
	end

end

optparser.parse!

# Checking that the user at least supplied the name of the sqlite file
#
if ARGV.length<1
	puts optparser 
	exit
end

db_file_name=ARGV.shift 
$db=SQLite3::Database.open (db_file_name)

create_mq_table($db)
create_pp_table($db)
create_msrun_table($db)
create_mssample_table($db)

$db.results_as_hash=true

def name_from_accession(accession)
	results=$db.execute("select LinkID from UniprotIndex where Accession=\"#{accession}\" AND AttributeType=\"D\"")
	results = results.collect { |r| r.values }.flatten.uniq
	results.join(" ").gsub(/RecName: Full=/,"")
end

def go_terms_from_accession(accession)
	results=$db.execute("select LinkID from UniprotIndex where Accession=\"#{accession}\" AND AttributeType=\"G\"")
	results = results.collect { |r| r.values }.flatten.uniq
	results.join(";")
end

# First just query all proteins for annotation information
# We add Sample and Run specific info later
protein_info={}

match_type = options[:by_transcript] ? "transcript" : "orf"

query_string = "
SELECT 
				ORF.orf_id AS orf_id,
				ORF.transcript_id AS transcript_id,
				BlastDbase.FullAccession AS accession,
				ORF.peptide AS peptide,
				BlastDbase.PercentIdentity AS percent_identity,
				min(BlastDbase.Evalue) AS e_value,
				length(peptide) AS pep_length,
				SignalP.prediction AS has_signalp,
				MaxQuant.abundance AS iBAQ
			FROM
				ProteinProphet
			 	JOIN ORF ON ORF.#{match_type}_id = ProteinProphet.protein_id
			 	LEFT JOIN BlastDbase ON ProteinProphet.protein_id = BlastDbase.TrinityID
			 	LEFT JOIN MaxQuant ON ProteinProphet.protein_id LIKE MaxQuant.indistinguishable_proteins
			 	LEFT JOIN SignalP ON SignalP.query_prot_id = ProteinProphet.protein_id

			GROUP BY orf_id
"

$db.execute(query_string) do |row|
	full_accession=row["accession"]

	unless full_accession.nil?
		accession_number=full_accession.split("|")[-2]
		accession_number=accession_number.split(".")[0]
		row["go_terms"] = go_terms_from_accession(accession_number)
		row["protein_name"] = name_from_accession(accession_number)
		row["uniprot_accession"] = accession_number
	end

	row["cysteines"] = row["peptide"].scan(/[Cc]/).length

	protein_info[row["#{match_type}_id"]]=row
end

header_fields = ["orf_id", "transcript_id","accession", "peptide", "percent_identity", 
				"e_value", "pep_length", "has_signalp",
				"go_terms", "protein_name","uniprot_accession",
				"group_prob",
				"protein_probability",
				"indistinguishable_proteins",
				"coverage",
				"iBAQ",
				"peptides",
				"num_peptides",
				"tissue",
				"separation",
				"enzyme",
				"instrument",
				"run_id"]

puts header_fields.join("\t")

$db.execute("SELECT
				protein_id,
				group_prob,
				protein_probability,
				indistinguishable_proteins,
				coverage,
				peptides,
				num_peptides,
				tissue,
				separation,
				enzyme,
				instrument,
				ProteinProphet.run_id
			FROM
				ProteinProphet
				JOIN MSRun ON ProteinProphet.run_id = MSRun.run_id
				JOIN MSSample ON MSRun.sample_name = MSSample.sample_name
			") do |row|
	row_info = protein_info[row["protein_id"]]
	unless row_info.nil?
		all_info = row_info.merge(row)
		row_fields = []
		header_fields.each { |key| row_fields << all_info[key] }
		puts row_fields.join("\t")
	end
end


