#This program will count the cystines within peptides.



#!/usr/bin/ruby

require 'sqlite3'

# Grab the peptide data from sqlite - (ORF, peptide)
# for each entry, the peptide, count the amount of Cs within in it 
#and post

db=SQLite3::Database.open ('/Users/nikeishacaruana/Desktop/Trinotate.sqlite')

count = db.execute("select length(peptide) - length(replace(peptide, 'C', '')) from ORF INSERT INTO ORF (cystine_count)")
#puts count

update ORF set cystine_count=length(peptide) - length(replace(peptide, 'C', ''))
	
update ORF set ORF_length=length(peptide)
