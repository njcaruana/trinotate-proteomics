# Incorporating proteomic data in a Trinotate sqlite database

The trinotate proteomics suite of tools includes scripts for the following tasks

- Importing proteomic data (from MaxQuant and/or the TPP) into a Trinotate.sqlite database
- Exporting ORF and CDS fasta files for use in proteomics pipelines
- Exporting GO annotations for use with topGO
- Calculating and exporting various properties of ORF relevant for toxin analysis

# Creating the _Trinotate.sqlite_ database

In order to use tools in this repository you first need a database of trinotate annotations. To do this use this [bpipe script](https://github.com/vlsci/bpipe-trinotate), or using the instructions on the trinotate website at http://trinotate.sourceforge.net/

# Create a protein database for protein tandem ms search

```bash
	orf_to_fasta.rb Trinotate.sqlite > orfs.fasta
```
# Load results from a ProteinProphet tabular file

For example if the results are from sample "S1" on tissue "PSG", and the run was a 6hr gradient with a tryptic digest on the Orbitrap.
This will automatically create entries in the sample and run tables as needed.

```bash
	load_proteinprophet.rb -s 'S1,PSG' -r '1,6hr,Trypsin,Orbi' Trinotate.sqlite protein_prophet_output.tabular 
```

MaxQuant Results can be loaded similarly, but the name of a quantitative measure is needed

```bash
	load_maxquant.rb -q 'iBAQ' -s 'S1,PSG' -r '1,6hr,Trypsin,Orbi' Trinotate.sqlite proteinGroups.txt
```

# Export result tables. 

At the moment we have a script to export results useful for toxin analysis. To use this just do 

```bash
	get_toxintable.rb Trinotate.sqlite > toxintable.txt
```

The resulting file `toxintable.txt` should be easily loadable in excel.

# Special instructions for working with 6-frame translation data

When loading data of this type you will need to convert protein ids to transcript ids as follows

```bash
	load_proteinprophet.rb -s 'S3,PSG' -r '1,6hr,Trypsin,Orbi' Trinotate.sqlite 6frame_protein_prophet_output.tabular --id-regex="(comp.*)_frame"
```

Then when exporting use

```bash
	get_toxintable.rb Trinotate.sqlite  -t > toxintable_6frame.txt
```

And typically this would be concatenated with a normal (non-6-frame) search as follows

```bash
	cat toxintable_6frame.txt toxintable.txt > toxintable_merged.txt
```

# Export a transcriptome

```bash
	transcript_to_fasta.rb Trinotate.sqlite > transcripts.fasta
```
